# Hardware setup and configuration

## Flashing Zigbee sniffer application

Connect the nRF52840 Development Kit using the J-Link USB port and flash the sniffer image by running the following scripts from the *firmware* folder:  
`./erase_all.sh`  
`./load_firmware_hex.sh nrf802154_sniffer.hex`

## Running Zigbee sniffer in Wireshark

Wireshark is alredy configured, just connect the flashed development kit to the host using the micro-USB J2 port.

To start Wireshark with the sniffer:  
1. Start Wireshark.  
2. Click the gear icon next to the nRF Sniffer for 802.15.4 capture interface. The interface options window appears.  
3. Select the channel and the correct serial port for the sniffer.  
4. Start the capture.  

## Flashing CLI application

Connect the nRF52840 Development Kit using the J-Link USB port and flash the CLI device image by running the following scripts from the *firmware* folder:  
`./erase_all.sh`  
`./load_firmware_hex.sh nrf52840_xxaa_cli.hex`

## IKEA bulb reset sequence

Toggle the power to bulb six times. The bulb should flash once after being turned on the sixth time.

# CLI test scripts description

CLI test scripts are used to communicate with a CLI dev-board
and to implement user specific test scenarios.

## Modules that implement base functionality to developing test scripts

### `zb_cli_test_base.py`

A base class to write CLI test scripts. You need to inheritance the
`ZbCliTestBase` class to implement specific test scenario.

There are two main features in the `ZbCliTestBase` class:
1. Callback queue - to schedule and run user specific callback functions
to communicate with a CLI-device and to send Zigbee packets;
2. ZBOSS signal handler - to receive and handle the ZBOSS signals.

For example, you can re-implement `zboss_signal_handler()` to schedule a callback function to send
some commands after the *Device Announcement* or the *Device Authorized* signals receiving.

### `zb_cli_test_constants.py`

Constants for CLI tests such as test script duration, ZBOSS signal types and parameters, etc.

### `zb_cli_test_device.py`

Auxiliary module to store information about connected devices.

See usage example in the `zb_cli_test_zcl.py` test script.

## Basic test scripts.

You can run them from the command line.

### `zb_cli_test_network_formation.py`

The test script implements network formation on a CLI-device.

Usage example:  
**./zb_cli_test_network_formation.py** *[SERIAL_NUM] [ROLE] [CHANNELS] [DURATION]*  
*[SERIAL_NUM]* - nine digit serial number of a Nordic dev-board  
*[ROLE]* - role for a CLI-device - must be **zc** or **zr**, **zed** role is not supported  
*[CHANNELS]* - channels list for a CLI-device; valid values: from **11** to **26**  
*[DURATION]* - the test script duration in seconds - when it is expired, the test script will be terminated  

Start a CLI-device as coordinator on the dev-board with *123456789* serial number on the *21 channel*
and terminate the test script after *1 second* from start.  
`./zb_cli_test_network_formation.py 123456789 zc 21 1`

Start a CLI-device as router on the dev-board with *123456789* serial number on the *11, 15 and 21 channels*  
and stay in an *infinity loop* after start.  
`./zb_cli_test_network_formation.py 123456789 zr 11,15,21 -1`

### `zb_cli_test_rejoin_leave.py`

The test script implements rejoin and leave an IKEA bulb.

First you must reset the CLI-device and the IKEA bulb, turn it off and run the
`zb_cli_test_network_formation.py` script to start a CLI-device
as coordinator.

Then you need to start a Nordic sniffer in Wireshark and run
this script on the same dev-board as described below.

Finally you can start the IKEA bulb.

When the IKEA bulb joins to the CLI-device we receive the
*Device Authorized* signal and schedule follow commands to send:
- *Leave* with *Rejoin* flag == True after 5 seconds;
- *Leave* with *Rejoin* flag == False after 15 seconds.

The IKEA bulb shall rejoin to the CLI-device after first *Leave* packet
and leave from the network after second one.

Usage example:  
**./zb_cli_test_rejoin_leave.py** *[SERIAL_NUM] [DURATION]*  
*[SERIAL_NUM]* - nine digit serial number of a Nordic dev-board  
*[DURATION]* - the test script duration in seconds,
when it is expired, the test script will be terminated

Create connection to a dev-board with *123456789* serial number and stay in an *infinity loop*  
`./zb_cli_test_rejoin_leave.py 123456789 -1`

Limitations:
- don't use duration value *less then 30 seconds*,
because the test script will be terminated earlier the
the second *Leave* command will be send.

### `zb_cli_test_zdo.py`

The test script send a *Match Descriptor* and a *Simple Descriptor* commands to an IKEA bulb.

First you must reset the CLI-device and the IKEA bulb, turn it off and
run the `zb_cli_test_network_formation.py` script to
start a CLI-device as coordinator.

Then you need to start a Nordic sniffer in Wireshark and run
this script on the same dev-board as described below.

When the IKEA bulb joins to the CLI-device we receive the
*Device Announcement* signal and schedule follow commands to send:
- *Match Descriptor* to get endpoints list;
- *Simple Descriptor* to get device information
by an endpoint from a *Match Descriptor Response*
(there is only one endpoint on the IKEA bulb).

Usage example:  
**./zb_cli_test_zdo.py** *[SERIAL_NUM] [DURATION]*  
*[SERIAL_NUM]* - nine digit serial number of a Nordic dev-board  
*[DURATION]* - the test script duration in seconds - when it is expired, the test script will be terminated

Create connection to a dev-board with *123456789* serial number and stay in an *infinity loop*  
`./zb_cli_test_zdo.py 123456789 -1`

Limitations:
- don't use the duration value *less then 30 seconds*, because the test script will be terminated earlier then the second *Simple Descriptor* command will be send.

### `zb_cli_test_zcl.py`

The test script implements simple scenario to communicate with an IKEA bulb - get a *Simple Descriptor* and send *OnOff* commands

First you must reset the CLI-device and the IKEA bulb, turn it off and
run the `zb_cli_test_network_formation.py` script to
start a CLI-device as coordinator.

Then you need to start a Nordic sniffer in Wireshark and run
this script on the same dev-board as described below.

When a child device joins to the CLI-device we receive the
*Device Authorized* signal, create a Device object using
correspond long and short addresses from the *Device Authorized*
signal and put this object to a list.

Then we send a *Match Descriptor* command to the child device,
get an endpoint number form a *Match Descriptor Response* and
send a *Simple Descriptor* command to this endpoint.

If information from a *Simple Descriptor Response* corresponds
to an IKEA bulb, we schedule follow commands for it:
- *Toggle* after 3 seconds;
- *On* after 6 seconds;
- *Off* after 9 seconds;
- *Toggle* after 12 seconds.

Usage example:  
**./zb_cli_test_zcl.py** *[SERIAL_NUM] [DURATION]*  
*[SERIAL_NUM]* - nine digit serial number of a Nordic dev-board  
*[DURATION]* - the test script duration in seconds - when it is expired, the test script will be terminated  

Create connection to a dev-board with *123456789* serial number and stay in an *infinity loop*  
`./zb_cli_test_zcl.py 123456789 -1`

Limitations:
- don't use duration value less then 40 seconds, because the test script will be terminated earlier the
the test scenario will be completed.

### `zb_cli_test_ota.py`

The test script implements simple scenario to communicate with an IKEA bulb -
request 'Simple Descriptor' and start OTA

First you must reset the CLI-device and the IKEA bulb, turn it off and
run the zb_cli_test_network_formation.py script to
start a CLI-device as coordinator.

Then you need to start the Zigbee sniffer in Wireshark and run
this script for the CLI-device as described below.

When a IKEA bulb will join to the CLI-device the 'Device Authorized' 
signal will be received. Then the Device object will be created using
correspond long and short addresses from the 'Device Authorized' signal.

Then the 'Match Descriptor' command will be send to the child device to
get an endpoint number form the 'Match Descriptor Response'. After that
the 'Simple Descriptor' command will be send to to this endpoint.

If the information from the 'Simple Descriptor Response' corresponds
to an IKEA bulb, the 'Image Notify' command will be scheduled to send.

Usage example:
**./zb_cli_test_ota.py** *[SERIAL_NUM] [DURATION]*
*[SERIAL_NUM]* - nine digit serial number of the nRF52840-DK board
*[DURATION]* - the test script duration in seconds - when it is expired, the test script will be terminated

Create connection to a dev-board with 123456789 serial number and stay in an infinity loop
`./zb_cli_test_ota.py 123456789 -1`

Limitations:
- don't use duration values less then 40 seconds, because the test script will be terminated earlier the
the test scenarion will be completed.

## Supported CLI commands

You can find the full command list of a CLI device by follow link:  
https://infocenter.nordicsemi.com/index.jsp?topic=%2Fsdk_tz_v3.1.0%2Fzigbee_example_cli_reference.html

***NOTE:*** not all of this commands are supported by zb_cli_wrapper!

You can find the detailed description of the CLI wrapper commands in the following sources:
- `zb_cli_wrapper/src/utils/cmd_wrappers/zigbee/bdb.py`
- `zb_cli_wrapper/src/utils/cmd_wrappers/zigbee/zdo.py`
- `zb_cli_wrapper/src/utils/cmd_wrappers/zigbee/zcl.py`

To run a specific command just call an appropriate function from `bdb.py`, `zdo.py` or `zcl.py`.
For example, if you want to send the *Read Attribute* command to the *OnOff* cluster, write something like this:

`LONG_ADDR = 1020304050abcdef`  
`ON_OFF_ATTR_ID = 0x0000`  
`SERVER_TO_CLIENT = 1`  
`ENDPOINT_NUM = 1`  
`HA_PROFILE = 0x0104`  
`self.cli_dev.zcl.readattr(LONG_ADDR, ON_OFF_ATTR_ID, direction=SERVER_TO_CLIENT, ep=ENDPOINT_NUM, profile=ENDPOINT_NUM, HA_PROFILE)`

## Short list of the supported commands in the Python CLI wrapper

### BDB

1. **bdb role** *\{role\}* - set or get the zigbee role of a device;
2. **bdb start** - start the commissioning process;
3. **bdb ic add** *\{ic\}* {eui64} - add information about the install code on the trust center;
4. **bdb ic set** *\{ic\}* - set install code on the device;
5. **bdb ic policy** *\{state\}* - set the trust center install code policy;
6. **bdb panid** *\{panid\}* - set or get the zigbee pan id value;
7. **bdb channel** - get the 802.15.4 channel;
8. **bdb channel** *\{bitmask\}* - set the 802.15.4 channel;
9. **bdb legacy** *\{state\}* - enable or disable the legacy device support;
10. **bdb factory_reset** - perform a factory reset via local action.

### ZDO

1. **zdo eui64** - get the EUI64 address of the Zigbee device;
2. **zdo short** - get the short 16-bit address of the Zigbee device;
3. **zdo mgmt_leave** *\{dst_addr\}* - send a request to a remote device to leave the network through zdo mgmt_leave_req;
4. **zdo match_desc** *\{dst_addr\} \{req_addr\} \{prof_id\} \{input_cluster_ids\} \{output_cluster_ids\}* - send match descriptor request;
5. **zdo simple_desc** *\{dst_addr\} \{ep\}* - send Simple Descriptor Request;
6. **zdo ieee_addr** *\{short_addr\}* - resolve the EUI64 address by sending the IEEE address request;
7. **zdo nwk_addr** *\{dst_eui64\}* - resolve the EUI64 address to a short network address;
8. **zdo bind on** *\{src_eui64\} \{src_ep\} \{dst_eui64\} \{dst_ep\} \{cluster\} \{dst_short\}* - create a binding between two endpoints on two nodes;
9. **zdo mgmt_bind** *\{short_addr\}* - read the binding table from a node.

### ZCL

1. **zcl ping** *\{eui64\} \{length\}* - issue a ping over ZCL;
2. **zcl attr read** *\{eui64\} \{ep\} \{cluster\} \{direction\} \{profile\} \{attr\}* - retrieve the attribute value of the remote node;
3. **zcl attr write** *\{eui64\} \{ep\} \{cluster\} \{direction\} \{profile\} \{attr_id:\} \{attr_type\} \{attr_value\}* - write the attribute value to the remote node;
4. **zcl cmd** *\{eui64\} \{ep\} \{cluster\} -p \{profile\} \{cmd_id\}* - send a generic command to the remote node without payload;
5. **zcl cmd** *\{eui64\} \{ep\} \{cluster\} -p \{profile\} \{cmd_id\} -l \{payload\}* - send a generic command to the remote node with payload;
6. **zcl subscribe on** *\{eui64\} \{ep\} \{cluster\} \{profile\} \{attr_id\} \{attr_type\} \{min_interval\} \{max_interval\}* - subscribe to the attribute changes on the remote node.
