"""Constants for CLI tests.
"""

from enum import Enum

class Duration(Enum):
    """Duration of a test script
    """

    ENDED = 0
    """To terminate a test script when duration is expired
    """

    INFINITY = -1
    """To stay in an infinity loop
    """

class AddrFormat(Enum):
    """Format of an address
    """

    DEFAULT = 0,
    """As a hex bytes string with ':' separator
    For example: 1:2:3:4:56:78:90:ab
    """

    HEX_STR = 1
    """As a hex bytes string without any separator
    For example: 010203040567890ab
    """

class ClusterDirection(Enum):
    """Direction of a ZCL packet
    """

    CLIENT_TO_SERVER = 0,
    """For clusters in server role
    """

    SERVER_TO_CLIENT = 1
    """For clusters in client role
    """

class ZbossSignalType(Enum):
    DEVICE_ANNCE = "2"
    """Notifies the application about the new device appearance.

    When generated:
    - upon receiving the Device_annce command.

    Signal parameters:
    - device_short_addr - uint16 - address of a device that recently joined to network
    """

    DEVICE_UPDATE = "22"
    """Notifies the Zigbee Trust center or parent router application about a new device
    joined, rejoined or left the network.

    When generated:
    - standard device secured rejoin;
    - standard device unsecured join;
    - device left;
    - standard device trust center rejoin.

    Signal parameters:
    - long_addr - uint8[8] - long address of the updated device
    - short_addr - uint16 - short address of the updated device
    - status - uint8 - indicates the updated status of the device,
    see @ZbossDeviceUpdateSignalStatus
    """

    DEVICE_AUTHORIZED = "23"
    """Notifies the Zigbee Trust center application about a new device is authorized
    in the network. For Zigbee R21 devices it means that the TCKL exchange procedure
    was finished, for the legacy devices it means that the
    Transport key was send to the device.

    When generated:
    - Authorization success
    - Authorization failed
    - Authorization timeout

    Signal parameters:
    - long_addr : uint8[8] - long address of the authorized device
    - short_addr : uint16 - short address of the authorized device
    - authorization_type : uint8 - status of the authorization procedure (depends on @ZbossAuthorizationType)
    - authorization_status : uint8 - Status of the authorization procedure (depends on @ZbossAuthorizationType)
    """

class ZbossDeviceUpdateSignalStatus(Enum):
    """Indicates the updated status of the device

    See r21 spec, 4.4.3.2 APSME-UPDATE-DEVICE.indication,
    Table 4.15 APSME-UPDATE-DEVICE.indication Parameters
    """

    SECURED_REJOIN = "0"
    """Standard device secured rejoin
    """

    UNSECURED_JOIN = "1"
    """Standard device unsecured join
    """

    DEVICE_LEFT = "2"
    """Device left
    """

    TRUST_CENTER_REJOIN = "3"
    """Standard device trust center rejoin
    """

class ZbossAuthorizationType(Enum):
    """Authorization types for @ZbossSignalType.DEVICE_AUTHORIZED
    """

    LEGACY = "0"
    """Authorization type for legacy devices ( < r21)
    """

    R21_TCLK = "1"
    """Authorization type for r21 device through TCLK
    """

class ZbossLegacyDeviceAuthorizationStatus(Enum):
    """Authorization statuses for @ZbossAuthorizationType.LEGACY
    """

    SUCCESS = "0"
    """Authorization success
    """

    FAILED = "1"
    """Authorization failed
    """

class ZbossTclkAuthorizationStatus(Enum):
    """Authorization statuses for @ZbossAuthorizationType.R21_TCLK
    """

    SUCCESS = "0"
    """Authorization success
    """

    TIMEOUT = "1"
    """Authorization timeout
    """

    FAILED = "2"
    """Authorization failed
    """

class OtaPayloadType(Enum):
    QUERY_JITTER = 0x00
    """Query jitter
    """

    MANUFACTURER_CODE = 0x01
    """Query jitter and manufacturer code
    """

    IMAGE_TYPE = 0x02
    """Query jitter, manufacturer code and image type
    """

    NEW_FILE_VERSION = 0x03
    """Query jitter, manufacturer code, image type and new file version
    """
