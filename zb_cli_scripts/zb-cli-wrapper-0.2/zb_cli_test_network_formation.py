#!/usr/bin/env python3

"""The test script implements network formation on CLI-device

Usage example:
./zb_cli_test_network_formation.py [SERIAL_NUM] [ROLE] [CHANNELS] [DURATION]
[SERIAL_NUM] - nine digit serial number of a Nordic nRF52840-DK board
[ROLE] -Zigbee role for a CLI-device - must be coordinator "zc" or router "zr". End device "zed" role is not supported
[CHANNELS] - channels list for a CLI-device; valid values: from 11 to 26
[DURATION] - the test script duration in seconds - when it is expired, the test script will be terminated

Start CLI-device as coordinator on the nRF52840-DK board with 123456789 serial number on the 21 channel
and terminate the test script after 1 second after start.
./zb_cli_test_network_formation.py 123456789 zc 21 1

Start CLI-device as a router on the nRF52840-DK board with 123456789 serial number on the 11, 15 and 21 channels
and stay in an infinity loop after start.
./zb_cli_test_network_formation.py 123456789 zr 11,15,21 -1
"""

import sys
import serial
import time
import logging
import argparse

from zb_cli_test_base import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(filename)s: %(levelname)s: %(message)s')

class ZbCliTestNetworkFormation(ZbCliTestBase):
    """Class to network formation on a CLI-device

    Implements simple scenario to start a CLI-device
    in a given role and on given channels.

    Limitations:
    - a CLI-device supports only 'zc' and 'zr' roles
    - channels must be from 11 to 26
    """

    def __init__(self, serial_num, duration_sec = Duration.INFINITY.value):
        """Consturct a class object

        Parameters
        ----------
        serial_num : str - serial number of a nordic dev-board
        duration_sec : float - duration of the test script
        """

        params = dict()
        params["segger"] = serial_num
        params["duration_sec"] = duration_sec

        ZbCliTestBase.__init__(self, params)

    def start_cl_device(self, role, channels):
        """Initialize and start a CLI-device

        Parameters
        ----------
        role : str - role to set on a CLI-device ('zc' or 'zr' only, 'zed' is not supported)
        channels : uint8[] - channels list to set on a CLI-device
        """

        try:
            self.params["role"] = role
            self.params["channels"] = channels

            self.cli_dev.bdb.role = role
            self.cli_dev.bdb.channel = channels

            self.cli_dev.bdb.start()
            self.cli_dev.bdb.legacy = True

            logging.info("CLI-device is started!")
        except:
            logging.error("Could not start CLI-device!")

            self.cli_dev.close_cli()
            self.cli_dev = None
            sys.exit()

def parse_args():
    """Parse command line arguments.

    Arguments
    ---------
    serial_num : str - Nordic dev-board serial number.
    role : str - role for a CLI-device, must be 'zc' or 'zr', 'zed' role is not supported.
    channels : uint8[] - channels list to start a CLI-device.
    duration_sec : float - the test script duration in seconds.
    The test script will be terminated when duration is expired, but you can use -1 to stay in an infinity loop.
    """

    parser = argparse.ArgumentParser(description="Start CLI-device")
    parser.add_argument("serial_num",
                        metavar="serial_num",
                        help="Nordic dev-board serial number")
    parser.add_argument("role",
                        metavar="role",
                        help="Device role: zc or zr")
    parser.add_argument("channels",
                        metavar="channels",
                        help="Channels list, for example: 21 or 15,18,22")
    parser.add_argument("duration_sec",
                        metavar="duration_sec",
                        help="Time to idle in seconds (-1 for infinity loop)")

    args = parser.parse_args()
    channels = list()
    valid_channels = range(11, 27)

    for channel in args.channels.split(","):
        channel = int(channel)
        if channel in valid_channels:
            channels.append(channel)
        else:
            logging.error("Channel '{}' is invalid! Valid channels are from 11 to 26!".format(channel))

    args.channels = channels
    args.duration_sec = int(args.duration_sec)

    return args

def main():
    args = parse_args()
    test = ZbCliTestNetworkFormation(args.serial_num, args.duration_sec)

    test.start_cl_device(args.role, args.channels)
    test.start()

if __name__ == "__main__":
    main()
