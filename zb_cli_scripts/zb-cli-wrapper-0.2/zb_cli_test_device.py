"""Auxiliary module to store information about connected devices.

See usage example in the zb_cli_test_zcl.py test script
"""

import sys
import logging
from enum import Enum
from zb_cli_test_constants import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(filename)s: %(levelname)s: %(message)s')

# Sample of Simple Descriptor params
# {
#    'src_addr': '0x7B56'',
#    'ep': '1',
#    'profile_id': '0xC05E', # HA Profile
#    'app_dev_id': '0x200',
#    'app_dev_ver': '0x2',
#    'in_clusters': [
#        '0x0000', # Basic
#        '0x0003', # Identify
#        '0x0004', # Groups
#        '0x0005', # Scenes
#        '0x0006', # OnOff
#        '0x0008', # Level Control
#        '0x0300', # Color Control
#        '0x0B05', # Unknown
#        '0x1000'  # ZLL Commissioning
#    ],
#    'out_clusters': [
#        '0x0005', # Scenes
#        '0x0019', # OTA Upgrade
#        '0x0020', # Poll Controll
#        '0x1000'  # ZLL Commissioning
#    ]
# }

class Endpoint(object):
    """Class to store endpoint information

    Attributes
    ----------
    num : uint8 - endpoint number
    profile_id : uint16 - profile ID
    app_dev_id : uint16 - application device ID
    app_dev_ver : uint16 - application version
    in_clusters : uint16[] - input cluster list (in server role)
    out_clusters : uint16[] - output cluster list (in client role)
    """

    def __init__(self, num):
        self.num = int(num)
        self.profile_id = None
        self.app_dev_id = None
        self.app_dev_ver = None
        self.in_clusters = None
        self.out_clusters = None

    def __init__(self, simple_desc_params:dict):
        """Consturct an Endpoint object and fill it using information from a Simple Descriptor      
        """

        self.num = int(simple_desc_params["ep"], 16)
        self.fill_by_simple_desc_params(simple_desc_params)

    def fill_by_simple_desc_params(self, params):
        """Fill endpoint information using a Simple Descriptor parameters

        Parameters
        ----------
        params : dict() - a Simple Descriptor parameters dictionary 
        """

        if self.num != int(params["ep"], 16):
            return

        self.profile_id = int(params["profile_id"], 16)
        self.app_dev_id = int(params["app_dev_id"], 16)
        self.app_dev_ver = int(params["app_dev_ver"], 16)

        self.in_clusters = list()
        for cluster in params["in_clusters"]:
            self.in_clusters.append(int(cluster, 16))

        self.out_clusters = list()
        for cluster in params["out_clusters"]:
            self.out_clusters.append(int(cluster, 16))

    def cluster_is_present( \
        self, \
        cluster:int, \
        cluster_direction:ClusterDirection = ClusterDirection.SERVER_TO_CLIENT):
        """Check a cluster in cluster lists

        Parameters
        ----------
        cluster : int - cluster ID to check
        cluster_direction : ClusterDirection - use ClusterDirection.CLIENT_TO_SERVER to check in
        'in_clusters' list, and ClusterDirection.SERVER_TO_CLIENT to check in 'out_clusters' list

        Returns
        -------
        bool - True if a cluster is present, False otherwise
        """

        clusters = \
            self.in_clusters \
            if cluster_direction == ClusterDirection.SERVER_TO_CLIENT \
            else self.out_clusters

        return cluster in clusters

class Device(object):
    """Class to store device information 

    Attributes
    ----------
    long_addr : uint8[8] - long address of a device
    short_addr : uint16 - short address of a device
    endpoints : list() - endpoints list of a device
    _params : dict() - a dictionary to store any user parameters,
    use set_param() and get_param() to setting and getting a parameter
    """

    def __init__(self, long_addr=None, short_addr=None):
        self.long_addr = long_addr
        self.short_addr = short_addr
        self.endpoints = list()
        self._params = dict()

    def update_endpoint_info(self, simple_desc_params):
        """Update information of an endpoint using parameters of a Simple Descriptor

        Doing nothing if the short address of a device is not equal
        the 'src_addr' field of a Simple Descriptor

        If there is no an endpoint in the '@self.endpoints' list which number is equal
        to an endpoint number of a Simple Descriptor, then new endpoint will be
        created and added to the '@self.endpoints' list
        """

        if int(self.short_addr, 16) != int(simple_desc_params["src_addr"], 16):
            logging.info("Could not update information of an endpoint!")
            logging.info("Device short_addr {} != Simple Descriptor src_addr {}" \
                         .format(self.short_addr, simple_desc_params["src_addr"]))
            return

        ep = self.get_endpoint(simple_desc_params["ep"])
        if ep is not None:
            ep.fill_by_simple_desc_params(simple_desc_params)
        else:
            ep = Endpoint(simple_desc_params)
            self.endpoints.append(ep)

    def cluster_is_present( \
        self, \
        cluster:int, \
        cluster_direction:ClusterDirection = ClusterDirection.SERVER_TO_CLIENT):
        """Check a cluster in the '@self.endpoints' list

        Parameters
        ----------
        cluster : uint16 - cluster ID to check
        cluster_direction : ClusterDirection - cluster direction

        Returns
        -------
        bool - True if there is an endpoint which contains a cluster in the appropriate role,
        False otherwise
        """

        for ep in self.endpoints:
            if ep.cluster_is_present(cluster, cluster_direction):
                return True
        return False

    def get_endpoint(self, endpoint_num:int):
        """Get an endpoint from the '@self.endpoints' by endpoint number

        Parameters
        ----------
        endpoint_num : int - number of an endpoint to find

        Returns
        -------
        Pointer to an Endpoint object if there is an endpoint in the endpoints list with endpoint_num,
        None otherwise
        """

        for ep in self.endpoints:
            if endpoint_num == ep.num:
                return ep
        return None

    def get_endpoint(self, cluster:int, cluster_direction = ClusterDirection.SERVER_TO_CLIENT):
        """Get an endpoint from the '@self.endpoints' that contains given cluster ID and cluster direction

        Parameters
        ----------
        cluster : uint16 - cluster ID
        cluster_direction : ClusterDirection - cluster direction

        Returns
        -------
        First pointer to an Endpoint object that contains a cluster in the appropriate role,
        None otherwise
        """

        for ep in self.endpoints:
            if ep.cluster_is_present(cluster, cluster_direction):
                return ep
        return None

    def get_long_addr(self, addr_format:AddrFormat = AddrFormat.DEFAULT):
        """Get the long address of a device in the appropriate format

        Parameters
        ----------
        addr_format : AddrFormat - address format

        Returns
        -------
        str - the long address of a device in the appropriate format
        """

        res = self.long_addr

        if addr_format == AddrFormat.HEX_STR:
            res = ""
            for byte in self.long_addr.split(":"):
                if len(byte) == 1:
                    res += "0"
                res += byte

        return res

    def set_param(self, key, value):
        """Set a user specific parameter to the '_params' dictionary

        Parameters
        ----------
        key - parameter name
        value - parameter value
        """

        self._params[key] = value

    def get_param(self, key):
        """Get a user specific parameter from the '_params' dictionary

        Parameters
        ----------
        key - parameter name

        Returns
        -------
        Parameter value, if a parameter is present,
        None otherwise
        """

        return self._params.get(key)
