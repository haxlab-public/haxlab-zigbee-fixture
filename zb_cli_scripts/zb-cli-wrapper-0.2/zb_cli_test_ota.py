#!/usr/bin/env python3

"""The test script implements simple scenario to communicate with an IKEA bulb -
request 'Simple Descriptor' and start OTA

First you must reset the CLI-device and the IKEA bulb, turn it off and
run the zb_cli_test_network_formation.py script to
start a CLI-device as coordinator.

Then you need to start the Zigbee sniffer in Wireshark and run
this script for the CLI-device as described below.

When a IKEA bulb will join to the CLI-device the 'Device Authorized' 
signal will be received. Then the Device object will be created using
correspond long and short addresses from the 'Device Authorized' signal.

Then the 'Match Descriptor' command will be send to the child device to
get an endpoint number form the 'Match Descriptor Response'. After that
the 'Simple Descriptor' command will be send to to this endpoint.

If the information from the 'Simple Descriptor Response' corresponds
to an IKEA bulb, the 'Image Notify' command will be scheduled to send.

Usage example:
./zb_cli_test_ota.py [SERIAL_NUM] [DURATION]
[SERIAL_NUM] - nine digit serial number of the nRF52840-DK board
[DURATION] - the test script duration in seconds - when it is expired, the test script will be terminated

Create connection to a dev-board with 123456789 serial number and stay in an infinity loop
./zb_cli_test_ota.py 123456789 -1

Limitations:
- don't use duration values less then 40 seconds, because the test script will be terminated earlier the
the test scenarion will be completed.
"""

import sys
import serial
import time
import logging
import argparse

from zb_cli_test_base import *
from zb_cli_test_device import *
from zb_cli_test_constants import *

from zb_cli_wrapper.zb_cli_dev import ZbCliDevice
from zb_cli_wrapper.src.utils.cmd_wrappers.zigbee.constants import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(filename)s: %(levelname)s: %(message)s')

class ZbCliTestOta(ZbCliTestBase):
    """Class that implements the 'OTA' scenario

    First run a CLI device as coordinator using zb_cli_test_network_formation.py.
    Then run this script using the same dev-board.
    Finaly reset and run an IKEA bulb.

    When the IKEA bulb will join to the network, the CLI device will schedule
    the 'Match Descriptor' command to send. 

    Then we get endpoint number from the 'Match Descriptor Response' and schedule
    the 'Simple Descriptor' command to get information about it.

    If the OTA cluster is present in the endpoint,
    we send the Image Notify command to start OTA.
    """

    MATCH_DESC_DELAY_SEC = 3.0
    """Delay in seconds to send the 'Match Descriptor' command
    """

    SIMPLE_DESC_DELAY_SEC = 3.0
    """Delay in seconds to send the 'Simple Descriptor' command
    """

    OTA_IMAGE_NOTIFY_DELAY_SEC = 3.0
    """Delay in seconds to send the 'Image Notify' command
    """
    
    UNKNOWN_DEVICE_NAME = "Unknown Device"
    """Default name for a new device
    """

    IKEA_BULB_NAME = "IKEA Bulb"
    """Default name for an IKEA bulb
    """

#    IKEA_BULB_PROFILE = ZLL_PROFILE
    IKEA_BULB_PROFILE = DEFAULT_ZIGBEE_PROFILE_ID
    """Default profile ID of an IKEA bulb
    """

    IKEA_BULB_QUERY_JITTER_VALUE = 0x05
    """Query Jitter value
    """

    def __init__(self, serial_num, duration_sec = Duration.INFINITY.value):
        """Consturct a class object

        Parameters
        --------------------------------
        serial_num - str - serial number of a nordic dev-board
        duration_sec - int - duration of the test script
        """

        self.match_desc_was_scheduled = False
        self.devices = dict()

        params = dict()
        params["segger"] = serial_num
        params["duration_sec"] = duration_sec

        ZbCliTestBase.__init__(self, params, cli_dev_already_started = True)

    def zboss_signal_handler(self, signal_params_str):
        """Reimplemented handler of the ZBOSS signals, see @ZbossSignalType

        Processes only the Device Authorized signal to schedule
        the 'Match Descriptor' command

        Parameters
        --------------------------------
        signal_params_str - signal parameters string
        """

        signal_type, params = self.parse_zboss_signal_params(signal_params_str)
        self.trace_zboss_signal(signal_type, params)

        if ZbossSignalType.DEVICE_AUTHORIZED.value == signal_type:
            self.dev_auth_handler(params)

    def dev_auth_handler(self, signal_params):
        """Reimplemented handler of the Device Authorized signal

        Doing nothing if there is an unknown device in the 'self.devices' list
        or device authorization is failed.

        Otherwise creates a new device (see @Device class), put it to the list
        and schedule the 'Match Descriptor' command for it.

        Parameters
        --------------------------------
        signal_params - signal parameters dictionary
        """

        logging.info(">>dev_auth_handler()")
        device = self.devices.get(self.UNKNOWN_DEVICE_NAME)

        if device is not None:
            return

        if ZbossLegacyDeviceAuthorizationStatus.FAILED.value \
           == signal_params["authorization_status"]:
            logging.info("Device authorization failed!")
            return

        logging.info("Unknown device authorized!")

        if self.match_desc_was_scheduled:
            return

        unknown_device = Device(signal_params["long_addr"], signal_params["short_addr"])
        self.devices[self.UNKNOWN_DEVICE_NAME] = unknown_device

        self.match_desc_was_scheduled = True
        self.schedule_match_desc(signal_params["short_addr"], self.MATCH_DESC_DELAY_SEC)

        logging.info("<<dev_auth_handler()")

    def schedule_match_desc(self, short_addr, delay = 0.0):
        """Schedule the 'Match Descriptor' command sending

        Parameters
        --------------------------------
        short_addr - destination short address
        delay - delay in seconds before sending
        """

        logging.info(">>schedule_match_desc()")
        logging.info("dst and req addr == 0x{}, delay == {}" \
                     .format(short_addr, delay))

        params = dict()
        params["dst_addr"] = int(short_addr, 16)
        params["req_addr"] = int(short_addr, 16)
        params["prof_id"] = self.IKEA_BULB_PROFILE
        params["input_cluster_ids"] = [0]
        params["output_cluster_ids"] = [0]

        self.schedule_cb(self.send_match_desc, params, delay)

        logging.info("<<schedule_match_desc()")

    def send_match_desc(self, params):
        """Send the 'Match Descriptor' command

        Takes endpoint number from a response and schedules
        the 'Simple Descriptor' command

        Parameters
        --------------------------------
        params - command parameters dictionary
        see https://infocenter.nordicsemi.com/index.jsp?topic=%2Fsdk_tz_v3.1.0%2Fzigbee_example_cli_reference.html
        """

        logging.info(">>send_match_desc()")
        logging.info("params: {}".format(params))

        responses = self.cli_dev.zdo.match_desc( \
            input_clusters=params["input_cluster_ids"], \
            output_clusters=params["output_cluster_ids"], \
            dst_addr=params["dst_addr"], \
            req_addr=params["req_addr"], \
            prof_id=params["prof_id"])

        logging.info("--------")
        logging.info("responses:")
        for resp in responses:
            addr_of_interest = int(resp[0], 16)
            endpoint = int(resp[1])

            logging.info("addr_of_interest: {}, endpoint: {}".format(hex(addr_of_interest), endpoint))

            if addr_of_interest == params["req_addr"]:
                self.schedule_simple_desc(addr_of_interest, endpoint, delay=self.SIMPLE_DESC_DELAY_SEC)
                break

        logging.info("<<send_match_desc()")

    def schedule_simple_desc(self, dst_addr, endpoint, delay = 0.0):
        """Schedule the 'Simple Descriptor' command sending

        Parameters
        --------------------------------
        dst_addr - destination adress
        endpoint - endpoint number
        delay - delay in seconds before sending
        """

        logging.info(">>schedule_simple_desc()")
        logging.info("dst_addr == {}, ep_num == {}, delay == {}" \
                     .format(hex(dst_addr), endpoint, delay))

        params = dict()
        params["dst_addr"] = dst_addr
        params["ep"] = endpoint

        self.schedule_cb(self.send_simple_desc, params, delay)

        logging.info("<< schedule_simple_desc()")

    def send_simple_desc(self, params):
        """Send the 'Simple Descriptor' command

        If parameters in a response are according an IKEA Bulb,
        then OTA will be started.

        Parameters
        --------------------------------
        params - command parameters dictionary
        see https://infocenter.nordicsemi.com/index.jsp?topic=%2Fsdk_tz_v3.1.0%2Fzigbee_example_cli_reference.html
        """

        logging.info(">>send_simple_desc()")
        logging.info("params: {}".format(params))

        unknown_device = self.devices.get(self.UNKNOWN_DEVICE_NAME)
        response = self.cli_dev.zdo.simple_desc_req(dst_addr=params["dst_addr"], ep=params["ep"])

        logging.info("response: {}".format(response))

        # simple_desc_resp_sample == {
        #     'src_addr': '0x7B56',
        #     'ep': '1',
        #     'profile_id': '0xC05E', # ZLL Profile
        #     'app_dev_id': '0x200',
        #     'app_dev_ver': '0x2',
        #     'in_clusters': [
        #         '0x0000', # Basic
        #         '0x0003', # Identify
        #         '0x0004', # Groups
        #         '0x0005', # Scenes
        #         '0x0006', # OnOff
        #         '0x0008', # Level Control
        #         '0x0300', # Color Control
        #         '0x0B05', # Unknown
        #         '0x1000'  # ZLL Commissioning
        #     ],
        #     'out_clusters': [
        #         '0x0005', # Scenes
        #         '0x0019', # OTA Upgrade
        #         '0x0020', # Poll Controll
        #         '0x1000'  # ZLL Commissioning
        #     ]
        # }

        if int(unknown_device.short_addr, 16) == int(response["src_addr"], 16):
            logging.info("IKEA Bulb was found!")

            ikea_bulb = self.devices.pop(self.UNKNOWN_DEVICE_NAME)
            ikea_bulb.update_endpoint_info(response)
            self.devices[self.IKEA_BULB_NAME] = ikea_bulb

            if unknown_device.cluster_is_present(OTA_CLUSTER, ClusterDirection.CLIENT_TO_SERVER):
                self.schedule_ota_image_notify_cmd( \
                    ikea_bulb, \
                    query_jitter=self.IKEA_BULB_QUERY_JITTER_VALUE, \
                    delay=self.OTA_IMAGE_NOTIFY_DELAY_SEC)

        logging.info("<<send_simple_desc()")

    def schedule_ota_image_notify_cmd(self, device, query_jitter=1, delay=5.0):
        """Schedule the 'Image Notify' cluster command sending

        Parameters
        --------------------------------
        device - destination device (see @Device class)
        query_jitter - query jitter value
        delay - delay in seconds before sending

        Image notify payload foramt (see ZCL spec rev. 7, 11.13.3 Image Notify Command):
        *------------*--------------*--------------*-------------------*------------*------------------*
        | Octets     | 1            | 1            | 0/2               | 0/2        | 0/4              |
        |------------|--------------|--------------|-------------------|------------|------------------|
        | Data Type  | enum8        | uint8        | uint16            | uint16     | uint32           |
        |------------|--------------|--------------|-------------------|------------|------------------|
        | Field Name | Payload type | Query jitter | Manufacturer code | Image type | New file version |
        *------------*--------------*--------------*-------------------*------------*------------------*
        """
        
        logging.info(">>schedule_ota_image_notify_cmd()")

        endpoint = device.get_endpoint( \
            cluster=OTA_CLUSTER, \
            cluster_direction=ClusterDirection.CLIENT_TO_SERVER)
        long_addr = int(device.get_long_addr(AddrFormat.HEX_STR), 16)

        params = dict()
        params["long_addr"] = long_addr
        params["ep_num"] = endpoint.num
        params["cluster"] = OTA_CLUSTER
        params["profile"] = DEFAULT_ZIGBEE_PROFILE_ID
        params["cmd_id"] = IMAGE_NOTIFY_CMD
        params["direction"] = ZCLDirection.DIRECTION_SRV_TO_CLI
        params["payload_type"] = OtaPayloadType.QUERY_JITTER.value
        params["query_jitter"] = query_jitter

        logging.info("params:")
        logging.info("long_addr == {}".format(hex(params["long_addr"])))
        logging.info("ep_num == {}".format(hex(params["ep_num"])))
        logging.info("cluster == {}".format(hex(params["cluster"])))
        logging.info("profile == {}".format(hex(params["profile"])))
        logging.info("cmd_id == {}".format(hex(params["cmd_id"])))
        logging.info("direction == {}".format(params["direction"]))
        logging.info("payload_type == {}".format(hex(params["payload_type"])))
        logging.info("query_jitter == {}".format(hex(params["query_jitter"])))

        self.schedule_cb(self.send_ota_image_notify_cmd, params, delay)
        logging.info("<<schedule_ota_image_notify_cmd()")

    def send_ota_image_notify_cmd(self, params):
        """Send the 'Image Notify' cluster command

        Parameters
        --------------------------------
        params - command parameters dictionary
        see https://infocenter.nordicsemi.com/index.jsp?topic=%2Fsdk_tz_v3.1.0%2Fzigbee_example_cli_reference.html
        """

        payload = list()
        payload.append( (params["payload_type"], UINT8_TYPE) )
        payload.append( (params["query_jitter"], UINT8_TYPE) )

        logging.info(">>send_ota_image_notify_cmd()")

        logging.info("params:")
        logging.info("long_addr == {}".format(hex(params["long_addr"])))
        logging.info("ep_num == {}".format(hex(params["ep_num"])))
        logging.info("cluster == {}".format(hex(params["cluster"])))
        logging.info("profile == {}".format(hex(params["profile"])))
        logging.info("cmd_id == {}".format(hex(params["cmd_id"])))
        logging.info("direction == {}".format(params["direction"]))
        logging.info("payload == {}".format(payload))
        logging.info("----------------")

        response = self.cli_dev.zcl.generic( \
            eui64=params["long_addr"], \
            ep=params["ep_num"], \
            cluster=params["cluster"], \
            profile=params["profile"], \
            cmd_id=params["cmd_id"], \
            direction=params["direction"], \
            payload=payload)

        logging.info("response: {}".format(response))
        logging.info("<<send_ota_image_notify_cmd()")

def parse_args():
    parser = argparse.ArgumentParser(description="Start CLI device")

    parser.add_argument("serial_num",
                        metavar="serial_num",
                        help="DevBoard serial number")
    parser.add_argument("duration_sec",
                        metavar="duration_sec",
                        help="Time to idle in seconds (-1 for infinity loop)")

    args = parser.parse_args()
    args.duration_sec = int(args.duration_sec)

    return args

def main():
    args = parse_args()
    test = ZbCliTestOta(args.serial_num, args.duration_sec)

    test.start()

if __name__ == "__main__":
    main()
