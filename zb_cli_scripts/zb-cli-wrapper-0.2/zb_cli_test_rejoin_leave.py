#!/usr/bin/env python3

"""The test script implements rejoin and leave an IKEA bulb.

First you must reset the CLI-device and the IKEA bulb, turn it off and run the
zb_cli_test_network_formation.py script to start a CLI-device
as coordinator.

Then you'll need to start Zigbee sniffer in Wireshark and run
this script on the CLI-device as described below.

Finally you can start the IKEA bulb.

When the IKEA bulb joins to the CLI-device the
'Device Authorized' signal will be received and the following set of commands will be scheduled:
- 'Leave' with 'Rejoin' flag == True after 5 seconds;
- 'Leave' with 'Rejoin' flag == False after 15 seconds.

The IKEA bulb shall rejoin to the CLI-device after first 'Leave' packet
and leave from the network after second one.

Usage example:
./zb_cli_test_rejoin_leave.py [SERIAL_NUM] [DURATION]
[SERIAL_NUM] - nine digit serial number of a Nordic dev-board
[DURATION] - the test script duration in seconds,
when it is expired, the test script will be terminated

Create connection to a dev-board with 123456789 serial number and stay in an infinity loop
./zb_cli_test_rejoin_leave.py 123456789 -1

Limitations:
- don't set the duration value less then 15 seconds,
because the test script will be terminated earlier then the
the second 'Leave' command will be send.
"""

import sys
import serial
import time
import logging
import argparse

from zb_cli_test_base import *
from zb_cli_wrapper.zb_cli_dev import ZbCliDevice

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(filename)s: %(levelname)s: %(message)s')

class ZbCliTestRejoinLeave(ZbCliTestBase):
    """Class that implements the 'RejoinLeave' scenario.

    First you must reset the IKEA bulb and turn it off.
    Then run the zb_cli_test_network_formation.py script to start a CLI-device as coordinator.
    Then start a Nordic sniffer in Wireshark and run this script on the same dev-board.
    Finally you can start the IKEA bulb.

    When the IKEA bulb joins to the CLI-device we receive the 'Device Authorized' signal
    and schedule follow commands to send:
    - 'Leave' with 'Rejoin' flag == True after 5 seconds;
    - 'Leave' with 'Rejoin' flag == False after 15 seconds.

    The IKEA bulb shall rejoin to the CLI-device after first 'Leave' packet
    and leave from the network after second one.
    """

    LEAVE_WITH_REJOIN_DELAY_SEC = 5.0
    """Delay in seconds to send a 'Leave' command with 'Rejoin' flag
    """

    LEAVE_WITHOUT_REJOIN_DELAY_SEC = 15.0
    """Delay in seconds to send a 'Leave' command without 'Rejoin' flag
    """

    def __init__(self, serial_num, duration_sec = Duration.INFINITY.value):
        """Consturct a class object

        Parameters
        ----------
        serial_num : str - serial number of a nordic dev-board
        duration_sec : float - duration of the test script
        """

        self.rejoin_was_scheduled = False

        params = dict()
        params["segger"] = serial_num
        params["duration_sec"] = duration_sec

        ZbCliTestBase.__init__(self, params, cli_dev_already_started = True)

    def zboss_signal_handler(self, signal_params_str):
        """Reimplemented handler of the ZBOSS signals

        Process only the 'Device Authorized' signal to schedule two 'Leave' commands:
        one with 'Rejoin' flag and another one without 'Rejoin' flag.

        Parameters
        ----------
        signal_params_str - signal parameters string
        """

        signal_type, params = self.parse_zboss_signal_params(signal_params_str)
        self.trace_zboss_signal(signal_type, params)

        if ZbossSignalType.DEVICE_AUTHORIZED.value == signal_type:
            self.dev_auth_handler(params)

    def dev_auth_handler(self, signal_params):
        """Reimplemented handler of the 'Device Authorized' signal

        Schedule two 'Leave' commands:
        one with 'Rejoin' flag and another one without 'Rejoin' flag.

        Process only first signal! After second one doing nothing.

        Parameters
        ----------
        signal_params - signal parameters dictionary
            signal_params["long_addr"] : uint8[8] - long address of the authorized device
            signal_params["short_addr"] : uint16 - short address of the authorized device
            signal_params["authorization_type"] : uint8 - status of the authorization procedure (depends on ZbossAuthorizationType)
            signal_params["authorization_status"] : uint8 - Status of the authorization procedure (depends on ZbossAuthorizationType)
        """

        logging.info(">>dev_auth_handler()")

        if self.rejoin_was_scheduled:
            return

        params = dict()
        params["short_addr"] = int(signal_params["short_addr"], 16)
        params["rejoin"] = True
        self.schedule_cb(cb = self.send_mgmt_leave, params = params, delay = self.LEAVE_WITH_REJOIN_DELAY_SEC)
        logging.info("schedule mgmt_leave, params == {}, delay == {}" \
                     .format(params, self.LEAVE_WITH_REJOIN_DELAY_SEC))

        params = dict()
        params["short_addr"] = int(signal_params["short_addr"], 16)
        params["rejoin"] = False
        self.schedule_cb(cb = self.send_mgmt_leave, params = params, delay = self.LEAVE_WITHOUT_REJOIN_DELAY_SEC)
        logging.info("schedule mgmt_leave, params == {}, delay == {}" \
                     .format(params, self.LEAVE_WITHOUT_REJOIN_DELAY_SEC))

        self.rejoin_was_scheduled = True

        logging.info("<<dev_auth_handler()")

    def send_mgmt_leave(self, params):
        """Send a 'Leave' command

        Parameters
        ----------
        params : dict() - parameters dictionary of a 'Leave' command:
            params["short_addr"] - destination address
            params["rejoin"] - set 'True' to asking a destination device to perform rejoin after leave
            params["children"] - set 'True' to asking a destination device to remove itself and all
            its children from the network.
        """

        logging.info(">>send_mgmt_leave()")
        logging.info("params: {}".format(params))

        response = self.cli_dev.zdo.mgmt_leave(params["short_addr"], rejoin=params["rejoin"])
        logging.info("response: {}".format(response))

        logging.info("<<send_mgmt_leave()")

def parse_args():
    """Parse command line arguments.

    Arguments
    ---------
    serial_num : str - Nordic dev-board serial number.
    duration_sec : float - the test script duration in seconds.
    The test script will be terminated when duration is expired, but you can use -1 to stay in an infinity loop.
    """

    parser = argparse.ArgumentParser(description="Start CLI-device")

    parser.add_argument("serial_num",
                        metavar="serial_num",
                        help="DevBoard serial number")
    parser.add_argument("duration_sec",
                        metavar="duration_sec",
                        help="Time to idle in seconds (-1 for infinity loop)")

    args = parser.parse_args()
    args.duration_sec = int(args.duration_sec)

    return args

def main():
    args = parse_args()
    test = ZbCliTestRejoinLeave(args.serial_num, args.duration_sec)

    test.start()

if __name__ == "__main__":
    main()
