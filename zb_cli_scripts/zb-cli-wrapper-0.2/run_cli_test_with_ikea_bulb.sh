#!/bin/bash

VALID_TEST_LIST=("zb_cli_test_rejoin_leave" "zb_cli_test_zcl" "zb_cli_test_zdo" "zb_cli_test_ota")

DEV_BOARD_SERIAL_NUM=683776182

DEFAULT_ROLE=zc
DEFAULT_CHANNEL=11
DEFAULT_DURATION=-1

NETWORK_FORMATION_DURATION=1
NETWORK_FORMATION_TEST="zb_cli_test_network_formation.py ${DEV_BOARD_SERIAL_NUM} ${DEFAULT_ROLE} ${DEFAULT_CHANNEL} ${NETWORK_FORMATION_DURATION}"

BULB_SCRIPTS_PATH=/home/researcher1/git/zigbee-training-fixture/
BULB_POWER_ON_CMD=${BULB_SCRIPTS_PATH}/BulbPowerOn.sh
BULB_POWER_OFF_CMD=${BULB_SCRIPTS_PATH}/BulbPowerOff.sh
BULB_POWER_TOGGLE_CMD=${BULB_SCRIPTS_PATH}/BulbPowerToggle.sh
# ----------------------------------------------------------------

function usage () {
    echo "The script does following sequence to run a CLI test:"
    echo "1) reset the dev-board with CLI"
    echo "2) reset the IKEA bulb and turn it OFF"
    echo "3) run the CLI-device as a coordinator on the 11 channel: ${NETWORK_FORMATION_TEST}"
    echo "4) run the appropriate CLI test, for example: ./zb_cli_test_zcl.py ${DEV_BOARD_SERIAL_NUM} ${DEFAULT_DURATION}"
    echo "5) turn the IKEA bulb ON"
    echo
    echo "./run_cli_test_with_ikea_bulb.sh [CLI_TEST_NAME]"
    echo "[CLI_TEST_NAME] - name of the CLI test script"
    echo
    echo "Valid test scripts: ${VALID_TEST_LIST}"
    echo
    echo "Usage examples:"
    echo "./run_cli_test_with_ikea_bulb.sh zb_cli_test_rejoin_leave"
    echo "or"
    echo "./run_cli_test_with_ikea_bulb.sh zb_cli_test_zcl.py"
}

function array_contains () { 
    local array="$1[@]"
    local seeking=$2
    local res=1
    for element in "${!array}"; do
        if [[ ${element} == ${seeking} ]]; then
            res=0
            break
        fi
    done
    echo ${res}
}
# ----------------------------------------------------------------

arg=$1

if [[ ${arg} == "-h" ]] || [[ ${arg} == "--help" ]]; then
    usage
    exit 0
fi
# ----------------------------------------------------------------

cli_test_script=${arg}
cli_test=`echo ${cli_test_script} | sed 's/\.py$//'`

status_code=$(array_contains VALID_TEST_LIST ${cli_test})

if [ ${status_code} -ne 0 ]; then
    echo "[ERROR] '${cli_test_script}' is invalid!"
    usage
    exit 1
fi
# ----------------------------------------------------------------

echo "reset the CLI-device..."
nrfjprog --snr ${DEV_BOARD_SERIAL_NUM} -r

status_code=$?
if [ ${status_code} -ne 0 ]; then
    echo "[ERROR] Could not reset the CLI-device! Serial number: ${DEV_BOARD_SERIAL_NUM}"
    exit 1
fi

echo "reset and turn OFF the IKEA bulb..."
#source ${BULB_POWER_TOGGLE_CMD}
#source ${BULB_POWER_OFF_CMD}

echo "start network formation:"
echo "./${NETWORK_FORMATION_TEST}"
./${NETWORK_FORMATION_TEST}

sleep ${NETWORK_FORMATION_DURATION}

cli_test_script="${cli_test}.py"
echo "run the CLI test:"
echo "./${cli_test_script} ${DEV_BOARD_SERIAL_NUM} ${DEFAULT_DURATION}"
./${cli_test_script} ${DEV_BOARD_SERIAL_NUM} ${DEFAULT_DURATION}
# ----------------------------------------------------------------
