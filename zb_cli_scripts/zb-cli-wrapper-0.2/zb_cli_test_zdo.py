#!/usr/bin/env python3

"""The test script sends 'Match Descriptor' and 'Simple Descriptor' commands to the IKEA bulb.

First you must reset the CLI-device and the IKEA bulb, turn it off and
run the zb_cli_test_network_formation.py script to
start a CLI-device as coordinator.

Then you need to start a Zigbee sniffer in Wireshark and run
this script for the CLI-device as described below.

When a IKEA bulb will join to the CLI-device the 'Device Announcement'
signal will be received. Then the following commands will be send:
- 'Match Descriptor' to get an endpoints list;
- 'Simple Descriptor' to get the device information
(BTW there is only one endpoint on the IKEA bulb).

Usage example:
./zb_cli_test_zdo.py [SERIAL_NUM] [DURATION]
[SERIAL_NUM] - nine digit serial number of a Nordic dev-board
[DURATION] - the test script duration in seconds - when it is expired, the test script will be terminated

Create connection to a dev-board with 123456789 serial number and stay in an infinity loop
./zb_cli_test_zdo.py 123456789 -1

Limitations:
- don't use duration value less then 6 seconds, because the test script will be terminated earlier the
the second 'Simple Descriptor' command will be send.
"""

import sys
import serial
import time
import logging
import argparse

from zb_cli_test_base import *
from zb_cli_wrapper.zb_cli_dev import ZbCliDevice
from zb_cli_wrapper.src.utils.cmd_wrappers.zigbee.constants import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(filename)s: %(levelname)s: %(message)s')

class ZbCliTestZdo(ZbCliTestBase):
    """Class that implements the 'ZDO' scenario

    First you must reset the IKEA bulb, turn it off and
    run the zb_cli_test_network_formation.py script to
    start a CLI-device as coordinator.

    Then you need to start a Nordic sniffer in Wireshark and run
    this script on the same dev-board as described below.

    When the IKEA bulb will join to the network, the CLI-device will schedule
    a 'Match Descriptor' command to send. 

    Then we get endpoint number from a 'Match Descriptor Response' and schedule
    a 'Simple Descriptor' command to get information about the IKEA bulb.
    """

    MATCH_DESC_DELAY_SEC = 3.0
    """Delay in seconds to send a 'Match Descriptor' command
    """

    SIMPLE_DESC_DELAY_SEC = 3.0
    """Delay in seconds to send a 'Simple Descriptor' command
    """

    def __init__(self, serial_num, duration_sec = Duration.INFINITY.value):
        """Consturct a class object

        Parameters
        ----------
        serial_num : str - serial number of a nordic dev-board
        duration_sec : float - duration of the test script
        """

        self.match_desc_was_scheduled = False

        params = dict()
        params["segger"] = serial_num
        params["duration_sec"] = duration_sec

        ZbCliTestBase.__init__(self, params, cli_dev_already_started = True)

    def zboss_signal_handler(self, signal_params_str):
        """Reimplemented handler of the ZBOSS signals

        Process only the 'Device Announcement' signal to schedule
        a 'Match Descriptor' command and a 'Simple Descriptor' command.

        Parameters
        ----------
        signal_params_str : str - signal parameters string
        """

        signal_type, params = self.parse_zboss_signal_params(signal_params_str)
        self.trace_zboss_signal(signal_type, params)

        if ZbossSignalType.DEVICE_ANNCE.value == signal_type:
            self.dev_annce_handler(params)

    def dev_annce_handler(self, signal_params):
        """Reimplemented handler of the Device Announcement signal

        Schedule a 'Match Descriptor' command and a 'Simple Descriptor' command.

        Process only first signal! After second one doing nothing.

        Parameters
        ----------
        signal_params : dict() - signal parameters dictionary
            signal_params_str["device_short_addr"] : uint16 - address of a device that recently joined to network
        """

        logging.info(">>dev_annce_handler()")

        if self.match_desc_was_scheduled:
            return

        self.match_desc_was_scheduled = True
        self.schedule_match_desc(signal_params["device_short_addr"], self.MATCH_DESC_DELAY_SEC)

        logging.info("<<dev_annce_handler()")

    def schedule_match_desc(self, short_addr, delay = 0.0):
        """Schedule a 'Match Descriptor' command sending

        Parameters
        ----------
        short_addr : uint16 - destination and required short address
        delay : float - delay in seconds before sending
        """

        logging.info(">>schedule_match_desc()")
        logging.info("short_addr == 0x{}, delay == {}".format(short_addr, delay))

        params = dict()
        params["dst_addr"] = int(short_addr, 16)
        params["req_addr"] = int(short_addr, 16)
        params["prof_id"] = ZLL_PROFILE
        params["input_cluster_ids"] = [0]
        params["output_cluster_ids"] = [0]

        self.schedule_cb(self.send_match_desc, params, delay)
        logging.info("<<schedule_match_desc()")

    def send_match_desc(self, params):
        """Send a 'Match Descriptor' command

        Take endpoint number from a response and schedule
        a 'Simple Descriptor' command

        Parameters
        ----------
        params : dict() - command parameters dictionary
            params["input_cluster_ids"] : uint16[] - inpit clusters list
            params["output_cluster_ids"] : uint16[] - output clusters list
            params["dst_addr"] : uint16 - destination address
            params["req_addr"] : uint16 - short address of interest
            params["prof_id"] : uint16 - profile ID
        """

        logging.info(">>send_match_desc()")
        logging.info("params: {}".format(params))

        responses = self.cli_dev.zdo.match_desc( \
            input_clusters=params["input_cluster_ids"], \
            output_clusters=params["output_cluster_ids"], \
            dst_addr=params["dst_addr"], \
            req_addr=params["req_addr"], \
            prof_id=params["prof_id"])

        logging.info("responses:")
        for resp in responses:
            logging.info("short_addr == {}, ep == {}".format(resp[0], resp[1]))
            logging.info("--------")

            if int(resp[0], 16) == params["req_addr"]:
                params = dict()
                params["dst_addr"] = int(resp[0], 16)
                params["ep"] = int(resp[1])

                self.schedule_cb(self.send_simple_desc, params, self.SIMPLE_DESC_DELAY_SEC)

        logging.info("<<send_match_desc()")

    def send_simple_desc(self, params):
        """Send a 'Simple Descriptor' command

        Parameters
        ----------
        params : dict() - command parameters dictionary
            params["dst_addr"] : uint16 - short address of interest
            params["ep"] : uint8 - endpoint number
        """

        logging.info(">>send_simple_desc()")
        logging.info("params: {}".format(params))

        response = self.cli_dev.zdo.simple_desc_req(dst_addr=params["dst_addr"], ep=params["ep"])

        logging.info("send_simple_desc response:")
        logging.info(response)
        logging.info("<<send_simple_desc()")

def parse_args():
    """Parse command line arguments.

    Arguments:
    ----------
    serial_num : str - Nordic dev-board serial number.
    duration_sec : float - the test script duration in seconds.
    The test script will be terminated when duration is expired, but you can use -1 to stay in an infinity loop.
    """

    parser = argparse.ArgumentParser(description="Start CLI-device")

    parser.add_argument("serial_num",
                        metavar="serial_num",
                        help="DevBoard serial number")
    parser.add_argument("duration_sec",
                        metavar="duration_sec",
                        help="Time to idle in seconds (-1 for infinity loop)")

    args = parser.parse_args()
    args.duration_sec = int(args.duration_sec)

    return args

def main():
    args = parse_args()
    test = ZbCliTestZdo(args.serial_num, args.duration_sec)

    test.start()

if __name__ == "__main__":
    main()
