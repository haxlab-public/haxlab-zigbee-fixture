"""This module implements a base class to write CLI test scripts.

You need to inheritance the ZbCliTestBase class to implement specific test scenario.

There are two main features in the ZbCliTestBase class:
1) Callback queue - to schedule and run user specific callback functions
to communicate with a CLI-device and to send Zigbee packets;
2) ZBOSS signal handler - to receive and handle the ZBOSS signals.

For example, you can reimplement zboss_signal_handler() to schedule a callback function to send
some commands after the 'Device Announcement' or the 'Device Authorized' signals receiving.
"""

import sys
import serial
import time
import logging
import threading
import queue

from enum import Enum
from zb_cli_wrapper.zb_cli_dev import ZbCliDevice
from zb_cli_test_constants import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(filename)s: %(levelname)s: %(message)s')

class ZbCliTestCallbackEntry(object):
    """
    Class to store callback information.

    Parameters
    ----------
    cb : ptr
        Pointer to a callback function.
    params : dict()
        Parameters dictionary for a callback function.
    delay : float
        Delay in seconds before a callback function running.
    """

    def __init__(self, cb, params, delay):
        self.cb = cb
        self.params = params
        self.delay = delay

class ZbCliTestBase(threading.Thread):
    """Base class for CLI test scripts.

    Implements functionality to schedule callbacks in FIFO queue
    and base handling\\tracing ZBOSS signals.

    You need to inheritance this class to implement specific test scenario.

    This class inheritance the Thread class, so don't
    forget to call the start() method to run a thread.

    Parameters
    ----------
    __cb_queue : Queue
        Callbacks queue.
    __IDLE_STEP_MS : float
        Milliseconds number to idle in an infinity loop.
    params : dict()
        Parameters dictionary (serial number, role, channel, etc.) to connect to a CLI-board.
    """

    def __init__(self, params: dict, cli_dev_already_started = False):
        """Construct a ZbCliTestBase object

        Parameters
        ----------
        params : dict()
            Parameters dictionary (serial number, role, channel, etc.) to connect to a CLI-board.
            params["role"] : str - a CLI-device role - must be "zc" or "zr" ("zed" role is not supported).
            params["channels"] : int[] - channels list - valid values: from 11 to 26.
            params["segger"] : str - nine digit serial number of a Nordic dev-board.
            params["duration_sec"] : str - a test script duration in seconds.
            The test script will be terminated when duration is expired.
            Use Duration.INFINITY value (-1) to stay in an infinity loop.
        cli_dev_already_started : bool
            Set True to check a CLI-device connection.
        """

        threading.Thread.__init__(self)
        self.__cb_queue = queue.Queue()
        self.__IDLE_STEP_MS = 0.025
        self.params = params

        try:
            self.cli_dev = ZbCliDevice(params, self.zboss_signal_handler)

            if cli_dev_already_started:
                self.cli_dev = self.cli_dev.wait_until_connected()
                self.cli_dev.bdb.legacy = True
        except serial.serialutil.SerialException:
            logging.error("Can not create CLI device")
            self.cli_dev.close_cli()
            self.cli_dev = None

        logging.info("CLI device is created!")

    def run(self):
        """Reimplemented method of the Thread class.

        Check the callback queue __cb_queue every __IDLE_STEP_MS
        milliseconds and run a callback if need.

        Terminate a test script when params["duration"] is expired.

        If params["duration"] is equal Duration.INFINITY (-1),
        then it will idle in an infinity loop.
        """

        while True:
            cb_q_size = self.__cb_queue.qsize()

            while cb_q_size:
                cb_q_size -= 1
                cb_entry = self.__cb_queue.get(block=True)

                if cb_entry is None:
                    logging.info("Item taken from the queue is None. Trying to close CLI.")
                    self.cli.close_cli()
                    sys.exit()

                if not isinstance(cb_entry, ZbCliTestCallbackEntry):
                    logging.info("Item taken from the queue is not of the ZbCliTestCallbackEntry type. Closing CLI.")
                    self.cli_dev.close_cli()
                    sys.exit()

                if cb_entry.delay <= 0:
                    if cb_entry.params is not None:
                        cb_entry.cb(cb_entry.params)
                    else:
                        cb_entry.cb()
                else:
                    cb_entry.delay -= self.__IDLE_STEP_MS
                    self.__cb_queue.put(cb_entry)

                self.__cb_queue.task_done()

            time.sleep(self.__IDLE_STEP_MS)

            if Duration.INFINITY.value == self.params["duration_sec"]:
                pass
            elif self.params["duration_sec"] >= Duration.ENDED.value:
                self.params["duration_sec"] -= self.__IDLE_STEP_MS

                if self.params["duration_sec"] < Duration.ENDED.value:
                    break

        logging.info("Idle time is ended!")
        self.cli_dev.close_cli()
        self.cli_dev = None
        sys.exit()

    def schedule_cb(self, cb, params=None, delay=0.0):
        """Construct a ZbCliTestCallbackEntry object and put it to the queue.

        Parameters
        ----------
        cb : ptr
            Pointer to a function.
        params : dict()
            Parameters dictionary to pass to a callback function.
        delay : float
            Time in seconds to postpone a callback function running.
        """

        if cb is None:
            logging.info("Could not schedule a callback! 'cb' is None!")
            return

        cb_entry = ZbCliTestCallbackEntry(cb, params, delay)
        self.__cb_queue.put(cb_entry)

    def zboss_signal_handler(self, signal_params):
        """Base handling of the ZBOSS signals (just parameters tracing).

        You can reimplement it in a child class for specific behavior.

        Parameters
        ----------
        signal_params : str
            A string that contains ZBOSS signal parameters in follow format:
            'signal_type=value; parameter_name_1=value; parameter_name_2=value; ... parameter_name_n=_value'
            Гse parse_zboss_signal_params() to parse it.
        """

        signal_type, params = self.parse_zboss_signal_params(signal_params)

        if ZbossSignalType.DEVICE_ANNCE.value == signal_type:
            self.dev_annce_handler(params)
        elif ZbossSignalType.DEVICE_UPDATE.value == signal_type:
            self.dev_upd_handler(params)
        elif ZbossSignalType.DEVICE_AUTHORIZED.value == signal_type:
            self.dev_auth_handler(params)
        else:
            logging.info("Handler for a signal '{}' is not implemented!".format(signal_type))

        self.trace_zboss_signal(signal_type, params)

    def parse_zboss_signal_params(self, params_str):
        """Parse ZBOSS signal parameters string.

        Parameters
        ----------
        params_str : str
            ZBOSS signal parameters string in follow format:
            'signal_type=value; parameter_name_1=value; parameter_name_2=value; ... parameter_name_n=_value'

        Returns
        -------
        tuple
            signal_type : ZbossSignalType - ZBOSS signal type.
            params_dict : dict() - ZBOSS signal parameters dictionary.
        """

        params_arr = params_str.split("; ")
        signal_type = params_arr.pop(0).split("=")[1]

        params_dict = dict()

        for param in params_arr:
            buf = param.split("=")
            params_dict[buf[0]] = buf[1]

        return signal_type, params_dict

    def dev_annce_handler(self, signal_params):
        """Default handler for the 'Device Announcement' signal.

        Parameters
        ----------
        signal_params : dict() - 'Device Announcement' signal parameters dictionary.
            device_short_addr : uint16 - address of a device that recently joined to network.
        """

        logging.info("dev_annce_handler() is not implemented!")

    def dev_upd_handler(self, signal_params):
        """Default handler for the 'Device Update' signal.

        Parameters
        ----------
        signal_params : dict() - 'Device Update' signal parameters dictionary.
            long_addr : uint8[8] - long address of the updated device.
            short_addr : uint16  - short Address of the updated device.
            status : ZbossDeviceUpdateSignalStatus - indicates the updated status of the device.
        """

        logging.info("dev_upd_handler() is not implemented!")

    def dev_auth_handler(self, signal_params):
        """Default handler for the 'Device Authorized' signal.

        Parameters
        ----------
        signal_params : dict() - 'Device Authorized' signal parameters dictionary.
            long_addr : uint8[8] - long address of the authorized device.
            short_addr : uint16  - short Address of the authorized device.
            authorization_type : ZbossAuthorizationType - type of the authorization procedure.
            authorization_status : uint8 - status of the authorization procedure (depends on ZbossAuthorizationType).
        """

        logging.info("dev_auth_handler() is not implemented!")

    def trace_zboss_signal(self, signal_type, params):
        """Trace ZBOSS signal parameters.

        Parameters
        ----------
        signal_type : ZbossSignalType - ZBOSS signal type.
        signal_params : dict() - ZBOSS signal parameters dictionary.
        """

        print_separator = True

        if ZbossSignalType.DEVICE_ANNCE.value == signal_type:
            self.trace_dev_annce_signal(params)
        elif ZbossSignalType.DEVICE_UPDATE.value == signal_type:
            self.trace_dev_upd_signal(params)
        elif ZbossSignalType.DEVICE_AUTHORIZED.value == signal_type:
            self.trace_dev_auth_signal(params)
        else:
            print_separator = False

        if print_separator:
            logging.info("--------------------------------")

    def trace_dev_annce_signal(self, signal_params):
        """Trace parameters of the 'Device Announcement' signal.

        Parameters
        ----------
        signal_params : dict() - 'Device Announcement' signal parameters dictionary.
            device_short_addr : uint16 - address of a device that recently joined to network.
        """

        logging.info("DEVICE ANNOUNCEMENT signal params:")
        logging.info("device_short_addr == 0x{}".format(signal_params["device_short_addr"]))

    def trace_dev_upd_signal(self, signal_params):
        """Trace parameters of the 'Device Update' signal.

        Parameters
        ----------
        signal_params : dict()  - 'Device Update' signal parameters dictionary.
            long_addr : uint8[8] - long address of the updated device.
            short_addr : uint16  - short Address of the updated device.
            status : ZbossDeviceUpdateSignalStatus - indicates the updated status of the device.
        """

        logging.info("DEVICE UPDATE signal params:")
        logging.info("long_addr == {}".format(signal_params["long_addr"]))
        logging.info("short_addr == 0x{}".format(signal_params["short_addr"]))

        status = "Unknown: {}".format(signal_params["status"])

        if ZbossDeviceUpdateSignalStatus.SECURED_REJOIN.value == signal_params["status"]:
            status = "Secured Rejoin"
        elif ZbossDeviceUpdateSignalStatus.UNSECURED_JOIN.value == signal_params["status"]:
            status = "Unsecured Rejoin"
        elif ZbossDeviceUpdateSignalStatus.DEVICE_LEFT.value == signal_params["status"]:
            status = "Device Left"
        elif ZbossDeviceUpdateSignalStatus.TRUST_CENTER_REJOIN.value == signal_params["status"]:
            status = "Trust Center Rejoin"

        logging.info("status == {}".format(status))

    def trace_dev_auth_signal(self, signal_params):
        """Trace parameters of the 'Device Authorized' signal

        Parameters
        ----------
        signal_params : dict() - 'Device Authorized' signal parameters dictionary
            long_addr : uint8[8] - long address of the authorized device.
            short_addr : uint16  - short Address of the authorized device.
            authorization_type : ZbossAuthorizationType - type of the authorization procedure.
            authorization_status : uint8 - status of the authorization procedure (depends on ZbossAuthorizationType).
        """

        logging.info("DEVICE AUTHORIZED signal params:")
        logging.info("long_addr == {}".format(signal_params["long_addr"]))
        logging.info("short_addr == 0x{}".format(signal_params["short_addr"]))

        authorization_type = "Unknown: {}".format(signal_params["authorization_type"])
        authorization_status = "Unknown: {}".format(signal_params["authorization_status"])

        if ZbossAuthorizationType.LEGACY.value == signal_params["authorization_type"]:
            authorization_type = "Legacy"

            if ZbossLegacyDeviceAuthorizationStatus.SUCCESS.value == signal_params["authorization_status"]:
                authorization_status = "Success"
            elif ZbossLegacyDeviceAuthorizationStatus.FAILED.value == signal_params["authorization_status"]:
                authorization_status = "Failed"
        elif ZbossAuthorizationType.R21_TCLK.value == signal_params["authorization_type"]:
            authorization_type = "R21 TCLK"

            if ZbossTclkAuthorizationStatus.SUCCESS.value == signal_params["authorization_status"]:
                authorization_status = "Success"
            elif ZbossTclkAuthorizationStatus.TIMEOUT.value == signal_params["authorization_status"]:
                authorization_status = "Timeout"
            elif ZbossTclkAuthorizationStatus.FAILED.value == signal_params["authorization_status"]:
                authorization_status = "Failed"

        logging.info("authorization_type == {}".format(authorization_type))
        logging.info("authorization_status == {}".format(authorization_status))
