#!/bin/bash

function usage () {
    echo "Usage: ./load_firmware_hex.sh filename.hex"
}

firmware=$1

if [ "${firmware}" == "" ]; then
    usage
    exit 1
fi

cp -f ${firmware} bin/custom_firmware.hex
res=$?

if [ ${res} != 0 ]; then
    echo "Bad file!"
    exit 1
fi

JLinkExe -device NRF52840_XXAA -if SWD -speed 4000 -commanderscript scripts/load_firmware_hex.jlink
rm -f bin\custom_firmware.hex
